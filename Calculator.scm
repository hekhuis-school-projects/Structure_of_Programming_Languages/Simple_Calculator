;;; CIS 343
;;; GVSU Winter 2017
;;; Kyle Hekhuis
;;; Simple calculator that supports addition, subtraction, multiplication,
;;; division, and modulo. Error checking for non-numeric operands and 
;;; invalid operators was not required. Run the program with (calc).

;;; Recursively loops itself and prompts the user for the operator
;;; to use on the given value as well as a second operand.
;;; Can exit program at anytime by typing 'END'.
(define (calculate value)
	(display "Current value: ")
	(display value)
	(newline)
	(display "Enter an operator: ")
	(let ( (operator (read) ) )
		(cond
			( (eqv? operator 'END)
				(display "Final value: ")
				(display value)
				(newline)
				"Program terminated."
			)
			(else
				(display "Enter an operand: ")
				(let ( (operand (read) ) )
					(cond
						( (eqv? operand 'END)
							(display "Final value: ")
							(display value)
							(newline)
							"Program terminated."
						)
						(else
							(case operator
								( (+) (calculate (+ value operand) ) )
								( (-) (calculate (- value operand) ) )
								( (*) (calculate (* value operand) ) )
								( (/)
									;;; Check for division by zero.
									(cond
										( (eqv? operand '0)
											(display "Can not divide by 0!")
											(newline)
											(calculate value)
										)
										(else (calculate (/ value operand) ) )
									)
								)
								( (%) (calculate (modulo value operand) ) )
								;;; If user didn't enter valid operator just 
								;;; stick with the current value.
								(else (calculate value) )
							)
						)
					)
				)
			)
		)
	)
)

;;; Runs the calculator and prompts the user for first operand.
;;; Terminates if user enters 'END'
(define (calc) 
	(display "Enter an operand: ")
	(let ( (operand (read) ) )
		(cond
			( (eqv? operand 'END) 
				"Program terminated."
			)
			(else (calculate operand) )
		)
	)
)