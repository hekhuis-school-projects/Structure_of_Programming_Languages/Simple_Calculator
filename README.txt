Simple Calculator
Language: Scheme
Authors: Kyle Hekhuis (https://gitlab.com/hekhuisk)
Class: CIS 343 - Structure of Programming Languages
Semester / Year: Winter 2017

Simple calculator that supports addition, subtraction, multiplication,
division, and modulo. Error checking for non-numeric operands and 
invalid operators was not required. Run the program with (calc).

Recursively loops itself and prompts the user for the operator
to use on the given value as well as a second operand.
Can exit program at anytime by typing 'END'.

Usage:
Run the file in a Scheme interpreter.